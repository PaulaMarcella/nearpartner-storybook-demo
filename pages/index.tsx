import type { NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import { useState } from "react";

import Squash from "hamburger-react";

import {
  Button,
  IconButton,
  Card,
  Image as NPImage,
  Dropdown,
  Navbar,
  Register,
  Input,
  CheckRadio,
  Select,
} from "@npm-nearpartner/nearpartner-ui-components";
import Script from "next/script";

const Home: NextPage = () => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div className="main">
      <Head>
        <title>Near Partner UI Components</title>
        <meta
          name="description"
          content="Example App for Near Partner UI Components"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Script src="https://kit.fontawesome.com/70db9c15ce.js"></Script>

      <div className="container">
        <h1 className="title">Near Partner UI Component Library</h1>
        <p className="text">
          This is an example app to demonstrate the use of the UI Components.
        </p>
        <p className="text">
          <IconButton
            backgroundColor="#caaf18"
            borderRadius={6}
            icon="fab fa-gitlab"
            outline
            size={1}
          />
          <strong>GitLab:</strong>{" "}
          <a href="https://gitlab.com/PaulaMarcella/nearpartner-storybook-demo">
            https://gitlab.com/PaulaMarcella/nearpartner-storybook-demo
          </a>
        </p>
      </div>

      {/* -------------Buttons----------  */}
      <div className="container">
        <h3 className="title">Buttons</h3>
        <div className="component-canvas">
          <Button backgroundColor="#eb1098" borderRadius={6}>
            Button
          </Button>
          <br />
          <Button backgroundColor="#eb1098" borderRadius={6} outline>
            Button
          </Button>
          <br />
          <div
            style={{
              display: "flex",
            }}
          >
            <IconButton
              backgroundColor="#1f8fd1"
              borderRadius={50}
              icon="fas fa-smile"
              shadow
            />
            &nbsp;
            <IconButton
              backgroundColor="#1f8fd1"
              borderRadius={50}
              icon="fas fa-heart"
              shadow
            />
            &nbsp;
            <IconButton
              backgroundColor="#1f8fd1"
              borderRadius={50}
              icon="fas fa-coffee"
              shadow
            />
          </div>
          <br />
          <IconButton
            backgroundColor="#1f8fd1"
            borderRadius={50}
            icon="far fa-hand-point-left"
            label="With Text"
            shadow
          />
        </div>
      </div>

      {/* -------------Navigation----------  */}
      <div className="container">
        <h3 className="title">Navigation</h3>
        <div
          className="component-canvas"
          style={{
            height: "400px",
          }}
        >
          <Navbar
            activeHoverColor="#2ba5ef"
            activeHoverEffect="border"
            height={80}
            logoHeight={80}
            padding={36}
            shadow
          >
            <div className="brand">
              <Link passHref href="/">
                <a className="logo">
                  <NPImage
                    alt="Logo"
                    borderRadius={0}
                    height="auto"
                    imgSrc="https://placeholderlogo.com/img/placeholder-logo-1.png"
                    width="100%"
                  />
                </a>
              </Link>
              <div className="mobile-menu-button">
                <Squash toggled={isOpen} toggle={setIsOpen} size={20} rounded />
              </div>
            </div>
            <div className={`nav ${!isOpen ? "mobile-nav-closed" : ""}`}>
              <div className="navlink">
                <Link href="/#">Navlink1</Link>
              </div>
              <div className="navlink">
                <Link href="/#">NavLink2</Link>
              </div>
              <Dropdown title="Dropdown1">
                <li className="sublink">
                  <Link href="/#">Sublink1</Link>
                </li>
              </Dropdown>
              <Dropdown title="Dropdown2">
                <li className="sublink">
                  <Link href="/#">Sublink1</Link>
                </li>
                <li className="sublink">
                  <Link href="/#">Sublink2</Link>
                </li>
                <li className="sublink">
                  <Link href="/#">Sublink3</Link>
                </li>
              </Dropdown>
            </div>
          </Navbar>
        </div>
      </div>

      {/* -------------Cards----------  */}
      <div className="container">
        <h3 className="title">Cards</h3>
        <div className="component-canvas">
          <Card
            borderColor="#c7c7c7"
            borderRadius={10}
            padding="15px"
            shadow
            width="300px"
          >
            <div className="card-img-container">
              <NPImage
                alt="Alternative Text"
                backgroudColor="#c7c7c7"
                borderRadius={0}
                height="auto"
                imgSrc="https://media-exp1.licdn.com/dms/image/C560BAQEgPEE_YVxVmw/company-logo_200_200/0/1519895419632?e=2159024400&v=beta&t=kjLJ5bbMokuzNrRgsJ4Q56SkxxnkHGKzBXY4y1I1OCU"
                width="100%"
              />
            </div>
            <div className="card-body">
              <div className="card-body-title">
                <h3>Card Vertical</h3>
              </div>
              <div className="card-body-text">
                <p>
                  The card text goes here. Lorem ipsum dolor sit, amet
                  consectetur adipisicing elit. Dicta quis aliquid quas animi
                  nisi doloremque?
                </p>
              </div>
              <div className="card-button-container">
                <Button backgroundColor="#eeda0e" borderRadius={6}>
                  Button
                </Button>
              </div>
            </div>
          </Card>
          <br />
          <Card
            borderColor="#c7c7c7"
            borderRadius={10}
            orientation="horizontal"
            padding="15px"
            shadow
            width="300px"
          >
            <div className="card-img-container">
              <NPImage
                alt="Alternative Text"
                backgroudColor="#c7c7c7"
                borderRadius={0}
                height="auto"
                imgSrc="http://4.bp.blogspot.com/-E58avH3Ni_k/Un1rh2kKU5I/AAAAAAAACNE/yWDVM7iRDao/s1600/lisbon-cat-in-window.jpg"
                width="100%"
              />
            </div>
            <div className="card-body">
              <div className="card-body-title">
                <h3>Card Horizontal</h3>
              </div>
              <div className="card-body-text">
                <p>
                  The card text goes here. Lorem ipsum dolor sit, amet
                  consectetur adipisicing elit. Dicta quis aliquid quas animi
                  nisi doloremque?
                </p>
              </div>
              <div className="card-button-container">
                <Button backgroundColor="#eeda0e" borderRadius={6}>
                  Button
                </Button>
              </div>
            </div>
          </Card>
        </div>
      </div>

      {/* -------------Forms----------  */}
      <div className="container">
        <h3 className="title">Forms</h3>
        <div className="component-canvas">
          <Register>
            <Input borderRadius={6} placeholder="Name" type="text" />
            <Input borderRadius={6} placeholder="Email" type="email" />
            <Select
              borderRadius={6}
              options={[
                {
                  label: "1",
                  value: "1",
                },
              ]}
            />
            <Input borderRadius={6} placeholder="Password" type="password" />
            <Input
              borderRadius={6}
              placeholder="Confirm Password"
              type="password"
            />
            <Input
              borderRadius={6}
              focusColor="#2ba5ef"
              placeholder="Placeholder"
              size={1}
              type="date"
            />
            <div>
              <CheckRadio type="checkbox" />
              <label htmlFor="">Confirm</label>
            </div>
            <br />
            <Button borderRadius={6} mobileFullWidth type="submit">
              Submit
            </Button>
          </Register>
        </div>
      </div>
    </div>
  );
};

export default Home;
